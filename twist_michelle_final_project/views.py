from django.shortcuts import redirect


def redirect_root_view(request):
    return redirect('beerinfo_beer_list_urlpattern')
