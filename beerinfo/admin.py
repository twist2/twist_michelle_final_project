from django.contrib import admin
from .models import Brewery, BeerClass, Staff, Customer, Bar, Beer, Purchase

admin.site.register(Brewery)
admin.site.register(BeerClass)
admin.site.register(Staff)
admin.site.register(Customer)
admin.site.register(Bar)
admin.site.register(Beer)
admin.site.register(Purchase)
