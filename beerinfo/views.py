from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.http.response import HttpResponse
from django.template import loader
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from beerinfo.utils import PageLinksMixin
#https://www.kaggle.com/nickhould/craft-cans
from beerinfo.forms import BreweryForm, StaffForm, CustomerForm, BeerClassForm, BeerForm, PurchaseForm, BarForm
from .models import (
    Bar,
    Brewery,
    Staff,
    Customer,
    BeerClass,
    Beer,
    Purchase,
)


class BreweryList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Brewery
    permission_required = 'beerinfo.view_brewery'


class BreweryDetail(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.view_brewery'

    def get(self,request, pk):
        brewery = get_object_or_404(
            Brewery,
            pk=pk
        )
        beer_list = brewery.beers.all()
        return render(
            request,
            'beerinfo/brewery_detail.html',
            {'brewery': brewery, 'beer_list': beer_list}
        )


class BreweryCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = BreweryForm
    model = Brewery
    permission_required = 'beerinfo.add_brewery'



class BreweryUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    form_class = BreweryForm
    model = Brewery
    template_name = 'beerinfo/brewery_update.html'
    permission_required = 'beerinfo.change_brewery'


class BreweryDelete(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.delete_brewery'

    def get(self, request, pk):
        brewery = self.get_object(pk)
        beers = brewery.beers.all()
        if beers.count() > 0:
            return render(
                request,
                'beerinfo/brewery_refuse_delete.html',
                {'brewery': brewery,
                 'beers': beers,
                 }
            )
        else:
            return render(
                request,
                'beerinfo/brewery_confirm_delete.html',
                {'brewery': brewery}
            )

    def get_object(self, pk):
        return get_object_or_404(
            Brewery,
            pk=pk)

    def post(self, request, pk):
        brewery = self.get_object(pk)
        brewery.delete()
        return redirect('beerinfo_brewery_list_urlpattern')


class StaffList(LoginRequiredMixin, PermissionRequiredMixin, PageLinksMixin, ListView):
    paginate_by = 25
    model = Staff
    permission_required = 'beerinfo.view_staff'


class StaffDetail(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.view_staff'
    def get(self,request, pk):
        staff = get_object_or_404(
            Staff,
            pk=pk
        )
        bar_list = staff.bars.all()
        return render(
            request,
            'beerinfo/staff_detail.html',
            {'staff': staff, 'bar_list': bar_list}
        )


class StaffCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = StaffForm
    model = Staff
    permission_required = 'beerinfo.add_staff'


class StaffUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    form_class = StaffForm
    model = Staff
    template_name = 'beerinfo/staff_update.html'
    permission_required = 'beerinfo.change_staff'


class StaffDelete(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.delete_staff'

    def get(self, request, pk):
        staff = self.get_object(pk)
        bars = staff.bars.all()
        if bars.count() > 0:
            return render(
                request,
                'beerinfo/staff_refuse_delete.html',
                {'staff': staff,
                 'bars': bars,
                 }
            )
        else:
            return render(
                request,
                'beerinfo/staff_confirm_delete.html',
                {'staff': staff}
            )

    def get_object(self, pk):
        return get_object_or_404(
            Staff,
            pk=pk)

    def post(self, request, pk):
        staff = self.get_object(pk)
        staff.delete()
        return redirect('beerinfo_staff_list_urlpattern')


class CustomerList(LoginRequiredMixin, PermissionRequiredMixin, PageLinksMixin, ListView):
    paginate_by = 25
    model = Customer
    permission_required = 'beerinfo.view_customer'


class CustomerDetail(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.view_customer'
    def get(self,request, pk):
        customer = get_object_or_404(
            Customer,
            pk=pk
        )
        purchase_list = customer.purchases.all()
        return render(
            request,
            'beerinfo/customer_detail.html',
            {'customer': customer, 'purchase_list': purchase_list}
        )


class CustomerCreate(LoginRequiredMixin, PermissionRequiredMixin,CreateView):
    form_class = CustomerForm
    model = Customer
    permission_required = 'beerinfo.add_customer'


class CustomerUpdate(LoginRequiredMixin, PermissionRequiredMixin,UpdateView):
    form_class = CustomerForm
    model = Customer
    template_name = 'beerinfo/customer_update.html'
    permission_required = 'beerinfo.change_customer'


class CustomerDelete(LoginRequiredMixin, PermissionRequiredMixin,View):
    permission_required = 'beerinfo.delete_customer'

    def get(self, request, pk):
        customer = self.get_object(pk)
        purchases = customer.purchases.all()
        if purchases.count() > 0:
            return render(
                request,
                'beerinfo/customer_refuse_delete.html',
                {'customer': customer,
                 'purchases': purchases,
                 }
            )
        else:
            return render(
                request,
                'beerinfo/customer_confirm_delete.html',
                {'customer': customer}
            )

    def get_object(self, pk):
        return get_object_or_404(
            Customer,
            pk=pk)

    def post(self, request, pk):
        customer = self.get_object(pk)
        customer.delete()
        return redirect('beerinfo_customer_list_urlpattern')


class BeerClassList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    permission_required = 'beerinfo.view_beerclass'
    template_name = 'beerinfo/beer_class_list.html'
    def get(self, request):
        return render(
            request,
            'beerinfo/beer_class_list.html',
            {'beer_class_list': BeerClass.objects.all()}
        )


class BeerClassDetail(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.view_beerclass'

    def get(self,request, pk):
        beer_class = get_object_or_404(
            BeerClass,
            pk=pk
        )
        beer_list = beer_class.beers.all()
        return render(
            request,
            'beerinfo/beer_class_detail.html',
            {'beer_class': beer_class, 'beer_list': beer_list}
        )


class BeerClassCreate(LoginRequiredMixin, PermissionRequiredMixin,CreateView):
    form_class = BeerClassForm
    template_name = 'beerinfo/beer_class_form.html'
    permission_required = 'beerinfo.add_beerclass'


class BeerClassUpdate(UpdateView):
    form_class = BeerClassForm
    model = BeerClass
    template_name = 'beerinfo/beer_class_update.html'
    permission_required = 'beerinfo.change_beerclass'


class BeerClassDelete(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.delete_beerclass'

    def get(self, request, pk):
        beer_class = self.get_object(pk)
        beers = beer_class.beers.all()
        if beers.count() > 0:
            return render(
                request,
                'beerinfo/beer_class_refuse_delete.html',
                {'beer_class': beer_class,
                 'beers': beers,
                 }
            )
        else:
            return render(
                request,
                'beerinfo/beer_class_confirm_delete.html',
                {'beer_class': beer_class}
            )

    def get_object(self, pk):
        return get_object_or_404(
            BeerClass,
            pk=pk)

    def post(self, request, pk):
        beer_class = self.get_object(pk)
        beer_class.delete()
        return redirect('beerinfo_beer_class_list_urlpattern')


class BeerList(LoginRequiredMixin, PermissionRequiredMixin, PageLinksMixin, ListView):
    paginate_by = 25
    model = Beer
    permission_required = 'beerinfo.view_beer'


class BeerDetail(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.view_beer'
    def get(self,request, pk):
        beer = get_object_or_404(
            Beer,
            pk=pk
        )
        brewery = beer.brewery
        beer_alc_content = beer.beer_alc_content
        beer_class = beer.beer_class
        bar_list = beer.bars.all()
        return render(
            request,
            'beerinfo/beer_detail.html',
            {'beer': beer, 'brewery': brewery, 'beer_alc_content':beer_alc_content, 'beer_class':beer_class, 'bar_list': bar_list}
    )


class BeerCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = BeerForm
    model = Beer
    permission_required = 'beerinfo.add_beer'


class BeerUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    form_class = BeerForm
    model = Beer
    template_name = 'beerinfo/beer_update.html'
    permission_required = 'beerinfo.change_beer'


class BeerDelete(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.delete_beer'

    def get(self, request, pk):
        beer = self.get_object(pk)
        bars = beer.bars.all()
        if bars.count() > 0:
            return render(
                request,
                'beerinfo/beer_refuse_delete.html',
                {'beer': beer,
                 'bars': bars,
                 }
            )
        else:
            return render(
                request,
                'beerinfo/beer_confirm_delete.html',
                {'beer': beer}
            )

    def get_object(self, pk):
        return get_object_or_404(
            Beer,
            pk=pk)

    def post(self, request, pk):
        beer = self.get_object(pk)
        beer.delete()
        return redirect('beerinfo_beer_list_urlpattern')


class PurchaseList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Purchase
    permission_required = 'beerinfo.view_purchase'


class PurchaseDetail(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.view_purchase'
    def get(self,request, pk):
        purchase = get_object_or_404(
            Purchase,
            pk=pk
        )
        customer = purchase.customer
        staff = purchase.staff
        bar = purchase.bar
        beer = purchase.beer
        total = purchase.total()
        return render(
            request,
            'beerinfo/purchase_detail.html',
            {'purchase': purchase, 'customer': customer, 'staff': staff, 'beer': beer, 'bar': bar, 'total': total}
    )


class PurchaseCreate(LoginRequiredMixin, PermissionRequiredMixin,CreateView):
    form_class = PurchaseForm
    model = Purchase
    permission_required = 'beerinfo.add_purchase'


class PurchaseUpdate(LoginRequiredMixin, PermissionRequiredMixin,UpdateView):
    form_class = PurchaseForm
    model = Purchase
    template_name = 'beerinfo/purchase_update.html'
    permission_required = 'beerinfo.change_purchase'


class PurchaseDelete(LoginRequiredMixin, PermissionRequiredMixin,View):
    permission_required = 'beerinfo.delete_purchase'

    def get(self, request, pk):
        purchase = self.get_object(pk)
        return render(
            request,
            'beerinfo/purchase_confirm_delete.html',
            {'purchase': purchase}
        )

    def get_object(self, pk):
        customer = get_object_or_404(
            Purchase,
            pk=pk
        )
        return customer

    def post(self, request, pk):
        purchase = self.get_object(pk)
        purchase.delete()
        return redirect('beerinfo_purchase_list_urlpattern')


class BarList(LoginRequiredMixin, PermissionRequiredMixin,ListView):
    model = Bar
    permission_required = 'beerinfo.view_bar'


class BarDetail(LoginRequiredMixin, PermissionRequiredMixin,View):
    permission_required = 'beerinfo.view_bar'
    def get(self,request, pk):
        bar = get_object_or_404(
            Bar,
            pk=pk
        )
        staff = bar.staff
        return render(
            request,
            'beerinfo/bar_detail.html',
            {'bar': bar, 'staff': staff, }
        )


class BarCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = BarForm
    model = Bar
    permission_required = 'beerinfo.add_bar'


class BarUpdate(LoginRequiredMixin, PermissionRequiredMixin,UpdateView):
    form_class = BarForm
    model = Bar
    template_name = 'beerinfo/bar_update.html'
    permission_required = 'beerinfo.change_bar'


class BarDelete(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'beerinfo.delete_bar'

    def get(self, request, pk):
        bar = self.get_object(pk)
        return render(
            request,
            'beerinfo/bar_confirm_delete.html',
            {'bar': bar}
        )

    def get_object(self, pk):
        beer = get_object_or_404(
            Bar,
            pk=pk
        )
        return customer

    def post(self, request, pk):
        bar = self.get_object(pk)
        bar.delete()
        return redirect('beerinfo_bar_list_urlpattern')
