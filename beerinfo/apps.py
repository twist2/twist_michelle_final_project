from django.apps import AppConfig


class BeerinfoConfig(AppConfig):
    name = 'beerinfo'
