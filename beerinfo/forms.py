from django import forms

from beerinfo.models import Brewery, BeerClass, Beer, Staff, Customer, Bar, Purchase


class BreweryForm(forms.ModelForm):
    class Meta:
        model = Brewery
        fields = '__all__'

    def clean_brewery_name(self):
        return self.cleaned_data['brewery_name'].strip()

    def clean_brewery_city(self):
        return self.cleaned_data['brewery_city'].strip()

    def clean_brewery_state(self):
        return self.cleaned_data['brewery_state'].strip()


class StaffForm(forms.ModelForm):
    class Meta:
        model = Staff
        fields = '__all__'

    def clean_first_name(self):
        return self.cleaned_data['first_name'].strip()

    def clean_last_name(self):
        return self.cleaned_data['last_name'].strip()


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'

    def clean_first_name(self):
        return self.cleaned_data['first_name'].strip()

    def clean_last_name(self):
        return self.cleaned_data['last_name'].strip()


class BarForm(forms.ModelForm):
    class Meta:
        model = Bar
        fields = '__all__'

    def clean_bar_name(self):
        return self.cleaned_data['bar_name'].strip()

    def clean_bar_city(self):
        return self.cleaned_data['bar_city'].strip()

    def clean_bar_state(self):
        return self.cleaned_data['bar_state'].strip()


class BeerClassForm(forms.ModelForm):
    class Meta:
        model = BeerClass
        fields = '__all__'

    def clean_beer_class_name(self):
        return self.cleaned_data['beer_class_name'].strip()


class BeerForm(forms.ModelForm):
    class Meta:
        model = Beer
        fields = '__all__'

    def clean_beer_name(self):
        return self.cleaned_data['beer_name'].strip()

    # def clean_beer_alc_content(self):
    #     return self.cleaned_data['beer_alc_content'].strip()
    #
    # def clean_beer_price(self):
    #     return self.cleaned_data['beer_price'].strip()
    #
    # def clean_beer_class(self):
    #     return self.cleaned_data['beer_class'].strip()


class PurchaseForm(forms.ModelForm):
    class Meta:
        model = Purchase
        fields = '__all__'





