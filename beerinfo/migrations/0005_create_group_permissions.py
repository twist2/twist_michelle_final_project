from __future__ import unicode_literals
from itertools import chain

from django.db import migrations


def populate_permissions_lists(apps):
    permission_class = apps.get_model('auth', 'Permission')

    brewery_permissions = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                             content_type__model='brewery')

    beerclass_permissions = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                          content_type__model='beerclass')

    beer_permissions = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                                  content_type__model='beer')

    bar_permissions = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                                  content_type__model='bar')

    customer_permissions = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                           content_type__model='customer')

    staff_permissions = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                         content_type__model='staff')

    purchase_permissions = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                          content_type__model='purchase')

    perm_view_brewery = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                           content_type__model='brewery',
                                                           codename='view_brewery')

    perm_view_beer_class = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                        content_type__model='beerclass',
                                                        codename='view_beerclass')

    perm_view_beer = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                               content_type__model='beer',
                                                               codename='view_beer')

    perm_view_bar = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                               content_type__model='bar',
                                                               codename='view_bar')

    perm_view_customer = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                         content_type__model='customer',
                                                         codename='view_customer')

    perm_view_staff = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                       content_type__model='staff',
                                                       codename='view_staff')

    perm_view_purchase = permission_class.objects.filter(content_type__app_label='beerinfo',
                                                        content_type__model='purchase',
                                                        codename='view_purchase')

    ci_user_permissions = chain(perm_view_brewery,
                                perm_view_beer_class,
                                perm_view_beer,
                                perm_view_bar,
                                perm_view_customer,
                                perm_view_staff,
                                perm_view_purchase)

    ci_assigner_permissions = chain(bar_permissions,
                                    customer_permissions,
                                    staff_permissions,
                                    purchase_permissions,
                                    perm_view_brewery,
                                    perm_view_beer_class,
                                    perm_view_beer)

    ci_brewmaster_permissions = chain(brewery_permissions,
                                      beerclass_permissions,
                                      beer_permissions,
                                      perm_view_bar,
                                      perm_view_customer,
                                      perm_view_staff,
                                      perm_view_purchase)

    my_groups_initialization_list = [
        {
            "name": "ci_user",
            "permissions_list": ci_user_permissions,
        },
        {
            "name": "ci_assigner",
            "permissions_list": ci_assigner_permissions,
        },
        {
            "name": "ci_brewmaster",
            "permissions_list": ci_brewmaster_permissions,
        },
    ]
    return my_groups_initialization_list


def add_group_permissions_data(apps, schema_editor):
    groups_initialization_list = populate_permissions_lists(apps)

    Group = apps.get_model('auth', 'Group')
    for group in groups_initialization_list:
        if group['permissions_list'] is not None:
            group_object = Group.objects.get(
                name=group['name']
            )
            group_object.permissions.set(group['permissions_list'])
            group_object.save()


def remove_group_permissions_data(apps, schema_editor):
    groups_initialization_list = populate_permissions_lists(apps)

    Group = apps.get_model('auth', 'Group')
    for group in groups_initialization_list:
        if group['permissions_list'] is not None:
            group_object = Group.objects.get(
                name=group['name']
            )
            list_of_permissions = group['permissions_list']
            for permission in list_of_permissions:
                group_object.permissions.remove(permission)
                group_object.save()


class Migration(migrations.Migration):
    dependencies = [
        ('beerinfo', '0004_create_groups'),
    ]

    operations = [
        migrations.RunPython(
            add_group_permissions_data,
            remove_group_permissions_data
        )
    ]
