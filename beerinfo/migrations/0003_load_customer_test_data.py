
from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations

CUSTOMERS = [
    {
        'first_name': 'Elizabeth',
        'last_name': 'Wilson',
    },
    {
        'first_name': 'Victor',
        'last_name': 'Lewis',
    },
    {
        'first_name': 'Christopher',
        'last_name': 'Paterson',
    },
    {
        'first_name': 'Nicola',
        'last_name': 'Powell',
    },
    {
        'first_name': 'Ava',
        'last_name': 'White',
    },
    {
        'first_name': 'Fiona',
        'last_name': 'Bower',
    },
    {
        'first_name': 'Owen',
        'last_name': 'Reid',
    },
    {
        'first_name': 'Lily',
        'last_name': 'Miller',
    },
    {
        'first_name': 'Deirdre',
        'last_name': 'Gill',
    },
    {
        'first_name': 'Neil',
        'last_name': 'Marshall',
    },
    {
        'first_name': 'Emily',
        'last_name': 'Sanderson',
    },
    {
        'first_name': 'Jessica',
        'last_name': 'King',
    },
    {
        'first_name': 'Adrian',
        'last_name': 'Ogden',
    },
    {
        'first_name': 'Gabrielle',
        'last_name': 'Lee',
    },
    {
        'first_name': 'Joshua',
        'last_name': 'Abraham',
    },
    {
        'first_name': 'Brian',
        'last_name': 'Hill',
    },
    {
        'first_name': 'Bernadette',
        'last_name': 'Buckland',
    },
    {
        'first_name': 'Theresa',
        'last_name': 'Thomson',
    },
    {
        'first_name': 'Ella',
        'last_name': 'Burgess',
    },
    {
        'first_name': 'Donna',
        'last_name': 'James',
    },
    {
        'first_name': 'Audrey',
        'last_name': 'Hudson',
    },
    {
        'first_name': 'Charles',
        'last_name': 'Newman',
    },
    {
        'first_name': 'Adrian',
        'last_name': 'Black',
    },
    {
        'first_name': 'Lucas',
        'last_name': 'Wright',
    },
    {
        'first_name': 'Dorothy',
        'last_name': 'McLean',
    },
    {
        'first_name': 'Stephen',
        'last_name': 'Glover',
    },
    {
        'first_name': 'Gabrielle',
        'last_name': 'Poole',
    },
    {
        'first_name': 'Owen',
        'last_name': 'Edmunds',
    },
    {
        'first_name': 'Amelia',
        'last_name': 'Bower',
        'nickname': ''
    },
    {
        'first_name': 'Ava',
        'last_name': 'Graham',
    },
    {
        'first_name': 'Adam',
        'last_name': 'Taylor',
    },
    {
        'first_name': 'Lily',
        'last_name': 'Ross',
    },
    {
        'first_name': 'Joseph',
        'last_name': 'Skinner',
    },
    {
        'first_name': 'Grace',
        'last_name': 'Greene',
    },
    {
        'first_name': 'Ryan',
        'last_name': 'Parr',
    },
    {
        'first_name': 'Peter',
        'last_name': 'Russell',
    },
    {
        'first_name': 'William',
        'last_name': 'Mills',
    },
    {
        'first_name': 'Lucas',
        'last_name': 'Sharp',
    },
    {
        'first_name': 'Chloe',
        'last_name': 'Anderson',
    },
    {
        'first_name': 'Lisa',
        'last_name': 'Bond',
    },
    {
        'first_name': 'Boris',
        'last_name': 'Paterson',
    },
    {
        'first_name': 'Sam',
        'last_name': 'Wilkins',
    },
    {
        'first_name': 'Max',
        'last_name': 'Lyman',
    },
    {
        'first_name': 'Una',
        'last_name': 'Pullman',
    },
    {
        'first_name': 'Austin',
        'last_name': 'Wilkins',
    },
    {
        'first_name': 'Lucas',
        'last_name': 'Powell',
    },
    {
        'first_name': 'Neil',
        'last_name': 'Grant',
    },
    {
        'first_name': 'Maria',
        'last_name': 'Piper',
    },
    {
        'first_name': 'Andrea',
        'last_name': 'Walsh',
    },
    {
        'first_name': 'Anne',
        'last_name': 'Clark',
    },
    {
        'first_name': 'Tim',
        'last_name': 'Nolan',
    },
    {
        'first_name': 'Victor',
        'last_name': 'Welch',
    },
    {
        'first_name': 'Alan',
        'last_name': 'Davidson',
    },
    {
        'first_name': 'Charles',
        'last_name': 'Churchill',
    },
    {
        'first_name': 'Jessica',
        'last_name': 'Duncan',
    },
    {
        'first_name': 'Lucas',
        'last_name': 'Martin',
    },
    {
        'first_name': 'Maria',
        'last_name': 'Arnold',
    },
    {
        'first_name': 'Sophie',
        'last_name': 'Clark',
    },
    {
        'first_name': 'Jacob',
        'last_name': 'Bailey',
    },
    {
        'first_name': 'Harry',
        'last_name': 'Churchill',
    },
    {
        'first_name': 'Karen',
        'last_name': 'Lyman',
    },
    {
        'first_name': 'Trevor',
        'last_name': 'Hunter',
    },
    {
        'first_name': 'Stephen',
        'last_name': 'Lee',
    },
    {
        'first_name': 'Vanessa',
        'last_name': 'Underwood',
    },
    {
        'first_name': 'Richard',
        'last_name': 'Kelly',
    },
    {
        'first_name': 'Amelia',
        'last_name': 'Campbell',
    },
    {
        'first_name': 'Zoe',
        'last_name': 'McLean',
    },
    {
        'first_name': 'Sam',
        'last_name': 'Forsyth',
    },
    {
        'first_name': 'Phil',
        'last_name': 'Wright',
    },
    {
        'first_name': 'Maria',
        'last_name': 'Hudson',
    },
    {
        'first_name': 'Diana',
        'last_name': 'Mackay',
    },
    {
        'first_name': 'Stewart',
        'last_name': 'Ellison',
    },
    {
        'first_name': 'Sue',
        'last_name': 'Edmunds',
    },
    {
        'first_name': 'Karen',
        'last_name': 'Ogden',
    },
    {
        'first_name': 'Carol',
        'last_name': 'Martin',
    },
    {
        'first_name': 'Oliver',
        'last_name': 'Wilkins',
    },
    {
        'first_name': 'Adam',
        'last_name': 'Bower',
    },
    {
        'first_name': 'Katherine',
        'last_name': 'Clarkson',
    },
    {
        'first_name': 'Carol',
        'last_name': 'Turner',
    },
    {
        'first_name': 'Ian',
        'last_name': 'James',
    },
    {
        'first_name': 'Una',
        'last_name': 'McLean',
    },
    {
        'first_name': 'Heather',
        'last_name': 'Clark',
    },
    {
        'first_name': 'Liam',
        'last_name': 'Ellison',
    },
    {
        'first_name': 'Madeleine',
        'last_name': 'Gill',
    },
    {
        'first_name': 'Warren',
        'last_name': 'Berry',
    },
    {
        'first_name': 'Yvonne',
        'last_name': 'Lyman',
    },
    {
        'first_name': 'Claire',
        'last_name': 'Abraham',
    },
    {
        'first_name': 'Andrea',
        'last_name': 'Hemmings',
    },
    {
        'first_name': 'Claire',
        'last_name': 'Clarkson',
    },
    {
        'first_name': 'Max',
        'last_name': 'Mackenzie',
    },
    {
        'first_name': 'Luke',
        'last_name': 'Ince',
    },
    {
        'first_name': 'Samantha',
        'last_name': 'Tucker',
    },
    {
        'first_name': 'James',
        'last_name': 'Lewis',
    },
    {
        'first_name': 'Wendy',
        'last_name': 'Cornish',
    },
    {
        'first_name': 'Evan',
        'last_name': 'Hamilton',
    },
    {
        'first_name': 'Caroline',
        'last_name': 'Lee',
    },
    {
        'first_name': 'Stephanie',
        'last_name': 'Smith',
    },
    {
        'first_name': 'Lily',
        'last_name': 'Jones',
    },
    {
        'first_name': 'Melanie',
        'last_name': 'Peake',
    },
    {
        'first_name': 'Richard',
        'last_name': 'Smith',
    },
    {
        'first_name': 'Neil',
        'last_name': 'Randall',
    },
    {
        'first_name': 'John',
        'last_name': 'Quinn',
    },
    {
        'first_name': 'Emma',
        'last_name': 'Bell',
    },
    {
        'first_name': 'Stephanie',
        'last_name': 'Forsyth',
    },
    {
        'first_name': 'Claire',
        'last_name': 'Young',
    },
    {
        'first_name': 'Felicity',
        'last_name': 'Avery',
    },
    {
        'first_name': 'Robert',
        'last_name': 'Glover',
    },
    {
        'first_name': 'Paul',
        'last_name': 'Tucker',
    },
    {
        'first_name': 'Abigail',
        'last_name': 'Morrison',
    },
    {
        'first_name': 'Carl',
        'last_name': 'Ross',
    },
    {
        'first_name': 'Jack',
        'last_name': 'Parr',
    },
    {
        'first_name': 'Owen',
        'last_name': 'Ogden',
    },
    {
        'first_name': 'Grace',
        'last_name': 'Ball',
    },
    {
        'first_name': 'Felicity',
        'last_name': 'Hill',
    },
    {
        'first_name': 'Dylan',
        'last_name': 'Fisher',
    },
    {
        'first_name': 'Angela',
        'last_name': 'Black',
    },
    {
        'first_name': 'Carolyn',
        'last_name': 'Murray',
    },
    {
        'first_name': 'Michelle',
        'last_name': 'Morrison',
    },
    {
        'first_name': 'Penelope',
        'last_name': 'Glover',
    },
    {
        'first_name': 'Frank',
        'last_name': 'Newman',
    },
    {
        'first_name': 'Penelope',
        'last_name': 'Blake',
    },
    {
        'first_name': 'Theresa',
        'last_name': 'Russell',
    },
    {
        'first_name': 'Trevor',
        'last_name': 'Cameron',
    },
    {
        'first_name': 'Stephen',
        'last_name': 'Cornish',
    },
    {
        'first_name': 'Anthony',
        'last_name': 'Parr',
    },

]


def add_customer_data(apps, schema_editor):
    customer_class = apps.get_model('beerinfo', 'Customer')
    for customer in CUSTOMERS:
        try:
            duplicate_object = customer_class.objects.get(
                first_name=customer['first_name'],
                last_name=customer['last_name'],
            )
            print('Duplicate student entry not added to students table:', customer['first_name'], customer['last_name'])
        except ObjectDoesNotExist:
            customer_object = customer_class.objects.create(
                first_name=customer['first_name'],
                last_name=customer['last_name'],
            )


def remove_customer_data(apps, schema_editor):
    customer_class = apps.get_model('beerinfo', 'Customer')
    for customer in CUSTOMERS:
        customer_object = customer_class.objects.get(
            first_name=customer['first_name'],
            last_name=customer['last_name'],
        )
        customer_object.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('beerinfo', '0002_load_staff_test_data'),
    ]

    operations = [
        migrations.RunPython(
            add_customer_data,
            remove_customer_data
        )
    ]
