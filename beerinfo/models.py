from django.db import models
from django.urls import reverse


class BeerClass(models.Model):
    beer_class_id = models.AutoField(primary_key=True)
    beer_class_name = models.CharField(max_length=45, unique=True)

    def __str__(self):
        return '%s' % self.beer_class_name

    def get_absolute_url(self):
        return reverse('beerinfo_beer_class_detail_urlpattern',
                       kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('beerinfo_beer_class_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('beerinfo_beer_class_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['beer_class_name']
        verbose_name_plural = "BeerClasses"


class Brewery(models.Model):
    brewery_id = models.AutoField(primary_key=True)
    brewery_name = models.CharField(max_length=45, unique=True)
    brewery_city = models.CharField(max_length=45)
    brewery_state = models.CharField(max_length=15)

    def __str__(self):
        return '%s, %s, %s' % (self.brewery_name, self.brewery_city, self.brewery_state)

    def get_absolute_url(self):
        return reverse('beerinfo_brewery_detail_urlpattern',
                       kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('beerinfo_brewery_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('beerinfo_brewery_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['brewery_name', 'brewery_city', 'brewery_state']
        verbose_name_plural = "Breweries"


class Staff(models.Model):
    staff_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)

    def __str__(self):
        return '%s, %s' % (self.last_name, self.first_name)

    def get_absolute_url(self):
        return reverse('beerinfo_staff_detail_urlpattern',
                       kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('beerinfo_staff_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('beerinfo_staff_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['last_name', 'first_name']
        unique_together = (('last_name', 'first_name'),)
        verbose_name_plural = "Staff"


class Customer(models.Model):
    customer_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)

    def __str__(self):
        return '%s, %s' % (self.last_name, self.first_name)

    def get_absolute_url(self):
        return reverse('beerinfo_customer_detail_urlpattern',
                       kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('beerinfo_customer_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('beerinfo_customer_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['last_name', 'first_name']
        unique_together = (('last_name', 'first_name'),)


class Beer(models.Model):
    beer_id = models.AutoField(primary_key=True)
    beer_name = models.CharField(max_length=45, unique=True)
    beer_alc_content = models.DecimalField(max_digits=3, decimal_places=1, null=True)
    beer_price = models.DecimalField(max_digits=3, decimal_places=2, null=True)
    beer_class = models.ForeignKey(BeerClass, related_name='beers', on_delete=models.PROTECT)
    brewery = models.ForeignKey(Brewery, related_name='beers', on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.beer_name} ({self.beer_class}, {self.beer_alc_content}%)'

    def get_absolute_url(self):
        return reverse('beerinfo_beer_detail_urlpattern',
                       kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('beerinfo_beer_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('beerinfo_beer_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['beer_name', 'beer_class__beer_class_name', 'beer_alc_content']
        #unique_together = (('semester', 'course', 'section_name'),)


class Bar(models.Model):
    bar_id = models.AutoField(primary_key=True)
    bar_name = models.CharField(max_length=45, unique=True)
    bar_city = models.CharField(max_length=45, default='')
    bar_state = models.CharField(max_length=2, default='')
    staff = models.ManyToManyField(Staff, related_name='bars', default='')
    beer = models.ManyToManyField(Beer, related_name='bars', default='')

    def __str__(self):
        return f'{self.bar_name}, {self.bar_city}, {self.bar_state}'

    def get_absolute_url(self):
        return reverse('beerinfo_bar_detail_urlpattern',
                       kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('beerinfo_bar_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('beerinfo_bar_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['bar_name', 'bar_city', 'bar_state']
        unique_together = (('bar_name', 'bar_city', 'bar_state'),)


class Purchase(models.Model):
    purchase_id = models.AutoField(primary_key=True)
    quantity = models.IntegerField()
    beer = models.ForeignKey(Beer, related_name='purchases', on_delete=models.PROTECT, default='')
    customer = models.ForeignKey(Customer, related_name='purchases', on_delete=models.PROTECT)
    staff = models.ForeignKey(Staff, related_name='purchases', on_delete=models.PROTECT)
    bar = models.ForeignKey(Bar, related_name='purchases', on_delete=models.PROTECT, default='')

    def get_absolute_url(self):
        return reverse('beerinfo_purchase_detail_urlpattern',
                       kwargs={'pk': self.pk})
    
    def get_update_url(self):
        return reverse('beerinfo_purchase_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('beerinfo_purchase_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def total(self):
        total = self.beer.beer_price * self.quantity
        # for item in self.beer.all():
        #     total += (item.beer.beer_price * item.quantity)
        return total

    def __str__(self):
        return f'Bartender {self.staff.first_name} to customer {self.customer.first_name}, total is ${self.total()} at {self.bar}.'
